---
group: kava
title: Implementation of development strategy
slug: implementation-of-development-strategy
menuimage: "menyy_3"
background: "#ACD3EF"
---

<Hero>

<RunningHeader>TALLINN 2035 DEVELOPMENT STRATEGY</RunningHeader>

## Implementation of development strategy

**Mission of the city organisation:** To make Tallinn the best place to live for the people staying here, a desired destination for people arriving here and a good place of departure for people who start here.

For this purpose, the management of Tallinn as an organisation is human-centred, transparent and cooperative. The city is managed on the basis of a development strategy and other development documents are based thereon. The development strategy is the social agreement of the citizens on what their city should be like in the future. The city communicates with the public and stakeholders in a manner that is open and understandable, and it is easy for people to communicate with the city in turn. The city organisation is an organisation that learns and works together, it is built in consideration of objectives and performance and its structure adapts over time. The management decisions of the city are based on knowledge and data. New approaches and new solutions are tested.

<LightBox previewImageUrl="https://img.youtube.com/vi/THQweBHJNWA/hqdefault.jpg">
  <iframe
    width="100%"
    src="https://www.youtube.com/embed/THQweBHJNWA?autoplay=1&yt:cc=on&hl=en&cc_lang_pref=en&cc_load_policy=1"
    frameborder="0"
    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
    allowfullscreen
  ></iframe>
</LightBox>

</Hero>

<Content>

## Framework of the city organisation

![Arengustrateegia elluviimise skeem](meedia/moodikud/seire_skeem_eng.svg)

In its activities, the city is driven by the expectations and needs of the citizens and development ambitions of the city, which have been set forth in the city's development strategy as a social agreement. The city's policymaking and management decisions are evidence-based, and international and national goals are also taken into consideration. The development strategy words the vision and strategic goals for the city and describes their achievement through the city’s fields of activity – which of the city’s activities form fields of action and how they help achieve the strategic goals. The city provides user-centric services in a variety of fields, which support the achievement of the city's strategic goals.

### Links between strategic goals and areas

Strategic goals cover all fields of action. Each area contributes to the achievement of several, often all, strategic goals. An overview of the links between strategic goals and areas is given in the following table, which shows the estimated extent to which specific areas contribute to the achievement of strategic goals.

<HorizontalLink
  id="arengustrateegia-elluviimine"
  leftTitle="Strategic goals"
  rightTitle="Fields of activity"
/>

### City's system of development documents

**The city's development strategy** sets strategic goals for nearly 15 years. Usually, the priorities for implementing the development strategy are reviewed after the local government elections and the section of action programmes and courses of action are updated above all. As a rule, strategic goals are set for 8-10 years, but they may be amended or modified during that time. While theoretically, the development strategy provides the grounds in preparation for district master plans, in practice, both documents are prepared in consideration of each other: the provisions of the master plans are taken into consideration when the development strategy is prepared. However, amendment of master plans is initiated if development directions are adopted with the development strategy that are not reflected in the master plans. The strategic goals set in the city's development strategy are detailed and implemented through the activities set forth in various specific development documents. The operational programme and budget strategy of the city's development strategy are prepared for a period of 4-5 years - the operational programme describes the paths and programmes for action towards achieving the strategic goals and the budget strategy is a financial plan for their implementation.

**Cross-sectoral horizontal development documents** specify the strategic goals and provide specific guidelines for the preparation of area development plans. Such development documents are: a roadmap for innovative solutions, the plan for sustainable energy economy, adaptation to climate change, and the development trends of circular economy and accessibility. The span of these topics may grow over time.

**The area development plans** specify the goals set in the development strategy and their implementation. They are prepared for the areas defined in the development strategy. An area development plan may cover one or several areas of activity of the city. An area may have a development plan if the goals of the area are not defined in sufficient detail in the city's development strategy. All of the city's areas of activity must have an **operational programme**, which provides an action plan for the achievement of the established goals that is more detailed than the operational programme of the development strategy. The operational programme for an area includes a list of the city's activities with deadlines, desired results and responsible parties for at least the next four budgetary years.

If necessary, **other development documents** (e.g. **programmes, plans**) are also prepared for the achievement of one or several sub-goals of the area or for detailing horizontal topics (support services).

The links between the development documents of the city are described in the following figure.

![Arengudokumentide skeem](meedia/moodikud/dokumendid_eng.svg)

### Monitoring

The development strategy sets objectives for the city as a whole, not only for the city as a local authority. Above all, this applies to the level of strategic goals. This means that the strategic goals of the city are based on the UN Sustainable Development Goals, the Green Paper of the European Union among other international agreements as well as the objectives of the Estonian state. Thus, the achievement of these goals not only depends on the local government, but on all stakeholders: the state, enterprises, non-profit associations and residents. Therefore, the indicators agreed at the level of the state are ordinarily used to assess the achievement of these targets. The most important indicators that are monitored in order to assess the achievement of goals are set out in the explanattory of the strategy. As these goals are complex, generally, no one or two indicators exist that fully describe the progress made in the achievement of the goal. Statistics are also monitored, surveys and polls may be ordered and so on. The assessment of the progress made in the achievement of a goal will be given to the new city council after the local government elections with advice on whether the goals need to be changed or specified.

Similarly, to strategic goals, more indicators are observed upon the assessment of the objectives of areas than are brought out as indicators. The entity responsible for the specific area monitors them every year and an overview with an assessment of the achievement of strategic goals is presented to the council.

The implementation of action programmes is assessed in the management report, which is part of the city's annual report. The methodology for the assessment of action programmes is constantly updated, taking into consideration the development of the capability to assess services.

<!---
<ListHeader>UN sustainable development goals</ListHeader>

<ImageGrid maxWidth="88px">

![YRO](meedia/yro/goal_eng_01.png)

![YRO](meedia/yro/goal_eng_02.png)

![YRO](meedia/yro/goal_eng_03.png)

![YRO](meedia/yro/goal_eng_04.png)

![YRO](meedia/yro/goal_eng_05.png)

![YRO](meedia/yro/goal_eng_06.png)

![YRO](meedia/yro/goal_eng_07.png)

![YRO](meedia/yro/goal_eng_08.png)

![YRO](meedia/yro/goal_eng_09.png)

![YRO](meedia/yro/goal_eng_10.png)

![YRO](meedia/yro/goal_eng_11.png)

![YRO](meedia/yro/goal_eng_12.png)

![YRO](meedia/yro/goal_eng_13.png)

![YRO](meedia/yro/goal_eng_14.png)

![YRO](meedia/yro/goal_eng_15.png)

![YRO](meedia/yro/goal_eng_16.png)

![YRO](meedia/yro/goal_eng_17.png)

![YRO](meedia/yro/goal_eng_18.png)

</ImageGrid>
--->

### Services

The provision of services is the main activity of the city organisation. Services means direct and indirect public services as well as services provided for the functioning of the organisation. This means that the exercising of public authority is also regarded as the provision of a service. Services are designed and provided by the service design principle, which means that services are human-centred and user-friendly, relevant and valuable for the user as well as efficient and sustainable from the service provider’s viewpoint.

### Values of the city organisation

![Linnaorganisatsiooni väärtuste skeem](meedia/moodikud/vaartused_eng.svg)

A city that looks to the future and is inclusive can only be created through cooperation based on trust. Trust emerges withshared **values** and when each representative of the city organisation – from the executives to the employees – bears these values.

The city organisation values **orientation on goals and a thirst for knowledge.** Orientation on goals also means effectiveness. The objective for the city is to create value for its residents. Every employee who wants to demonstrate excellent performance, and works hard to achieve the shared goals, creates trust and gives the city a good reputation. Every employee, especially every executive, is a mirror of the organisation and an ambassador for the city's values.

**Cooperation and independence** are important in everyday work. The city is looking for cooperation partners and co-workers based on its goals. People in the city's organisation see opportunities, look for innovative solutions, implement new practices and experiment, learn and develop, implement goals, are helpful and communicate openly. They cooperate with citizens' associations, enterprises and cultural and research institutions. An employee of the city can take smart risks, find solutions and has the courage to take responsibility. They question the activities that don't serve citizens and create no added value for them.

**Wisdom and courage** help create a city that looks to the future. Decisions are also made when the situation is unclear. The city organisation takes the time to clarify the root causes of problems. Solutions are found and decisions are made based on facts, not opinions or assumptions. People are not afraid to experiment because they know that a small mistake is cheaper than a big mistake and experimenting is a valuable learning process for the organisation. Every employee of the city considers self-development important and contributes to this every day.

The city's employees are trustworthy and keep an open mind. They are experts in their respective fields who know how to listen and reflect and keep their calm in difficult situations. The city's employees treat everyone equally and with respect, irrespective of their position, standard of living, gender or nationality and possible disagreements. An opponent is not regarded as an enemy, but their positions are heard in order to learn from them. The city's employees are genuinely interested in how citizens feel when living and going about their business in the city every day, what they want and what they strive for. Requesting feedback from citizens and partners is a natural part of the work of the city's employees. This makes it possible to find the best solutions for Tallinn as a whole, its residents and guests and the companies, associations and organisations operating in the city by providing professional and caring services.

## Action programmes of city management and support services

1. **Human-centred service design**

   The city organisation provides human-centred services, considering the needs and expectations of citizens. Services are provided based on an integrated service management concept, according to which the structure of the city organisation is built. All services of the city must pass the service design process. The service design process is coordinated across the city. City agencies that provide services are centrally supported in development, improvement and analysis of existing services and the development of new ones. The relevance of and compliance with the requirements of the services is checked every year and the services are redesigned, if necessary. The feedback received from stakeholders and the city's employees is used to analyse which services include excess bureaucracy, where service quality needs to be improved and where the content of the service needs to be changed. Tallinn's digital services are known all over the world; inspiring and leading by example.

   **Key courses of action:** 1)&nbsp;central coordination of services (including development of a uniform service management concept and establishment of a centre of excellence and a roundtable for services); 2)&nbsp;development of client-centric e-services and their introduction to citizens; 3)&nbsp;central provision of the city's basic IT infrastructure, information security and workstation services; 4)&nbsp;central provision of other support services (e.g. law and procurements) to the entire city organisation; and 5)&nbsp;implementation of environmental and quality management systems.

2. **Data-based management**

   Open data is convenient to use, accessible to everyone and safe. As much open data about the city is given into public use as possible. Data is clearly visualised and help people make critical choices: where to make their home, where to establish their company and what options are there for getting something done in their neighbourhood. The city collects data in a user-friendly manner, requesting data only once. Sharing objective and timely information is also the basis for successful inclusion of the public and stakeholders. The city collects as little data as possible and as much data as is necessary, storing the data prudently and following data protection rules. The management decisions of the city rely on wide-ranging data and decisions are also explained based on data. The city has a 3D model describing the organisation of transport, energy use, the physical environment and services as well as the processes essential for the city, which helps explain choices to citizens in a clear manner. Data analysis is a part of service design. City agencies have contemporary business analysis capability. The achievement of strategic goals as well as satisfaction with services and the efficiency of their provision are monitored based on data.

   **Key courses of action:** 1)&nbsp;development of the 3D model of Tallinn (digital twin); 2)&nbsp;efficient protection of personal data and other non-public information; 3)&nbsp;effective consolidation of the information belonging to the city and originating from external data sources and making this accessible; and 4) making the information required for management decisions easily and understandably accessible to decision-makers.

3. **Strategic and financial planning**

   The city’s governance is based on the social agreement between citizens, expressed in the development strategy. The development strategy is the base for master plans and sectoral development documents. Planning is a constant process during which plans are updated based on monitoring outcomes. The decision-making process is open and includes citizens. The city constantly keeps the public informed of its progress towards the achievement of its goals. The city cooperates with partners who express the interests of communities and resident groups, whether they are subdistrict groups, professional associations, companies or other organisations. Development, goals and actions are planned according to the city's financial position and financing capability. Financial planning is based on strategic goals. The budget strategy and sectoral development documents proceed from the development strategy, which in turn is the basis for preparing the budget. The financing of municipal agencies is based on performance and cost models, which ensure that the agencies have enough resources for everyday management and service development. Financial management is done in a competence centre, which guarantees the financial support required for management decisions of different levels and the organisation of quality financial management and accounting and provides quality accounting, tax accounting and financial planning services to the city organisation.

   **Key courses of action:** 1)&nbsp;an up-to-date system and monitoring of the city's development documents; 2)&nbsp;provision of financial services to entities belonging to the city's consolidation group; 3)&nbsp;organisation of roundtables and cooperation assemblies of city residents and citizens' associations and their participation in decision-making processes; and 4)&nbsp;supporting the activities of non-profit associations, implementation of an inclusive budget.

4. **Clear communication**

   The city has its own identity and is spoken about in a language of stories. The city is uniform in its messages and uses the most suitable and up-to-date channels for informing citizens. Necessary information and services reach people, not vice versa. Being in dialogue with citizens is a part of public communication and a part of each city employee's job.

   **Key courses of action:** 1)&nbsp;preservation and development of the city’s visual identity;&nbsp;2) organisation of the preparation and distribution of information materials about services; 3)&nbsp;organisation of assembling printed matter introducing the city; 4)&nbsp;a citywide weekly publication; and 5)&nbsp;making Tallinn more noticeable in foreign media.

5. **Quality management and competent employees**

   The task of the city organisation is to guarantee human-centric, optimal and smartly managed services for the city's residents, visitors and employees in addition to the good management of the city's property. The management and development of basic and support services have been concentrated in competence centres to guarantee an even quality. The city organisation is structured by goals. Dedicated and motivated teams ensure the uniformly high quality of governance. The managers of the city organisation are selected by way of competitions for fixed terms of office and they are regularly assessed. Tallinn is perceived as an organisation that is free of corruption and has a modern management culture.

   **Key courses of action:** 1)&nbsp;development of a human-centric city organisation; 2)&nbsp;implementation of central recruitment; 3)&nbsp;recruitment of results-oriented, competent and experienced executives; 4)&nbsp;target-oriented development of teams; 5)&nbsp;improvement of management and data quality; 6)&nbsp;implementation of performance appraisals of executives; 7)&nbsp;updating the salaries policy; 8)&nbsp;development of an inspiring working environment; and 9)&nbsp;development of the city employee portal.

6. **Regional and international cooperation**

   Tallinn cooperates actively mainly with Harju County and its local authorities in order to achieve a cohesive and well-functioning urban region. Cooperation is also developed with other local authorities and the state for achieving the development goals of Estonia. International cooperation supports the achievement of Tallinn's strategic goals and contributes to increasing international awareness of the city as well as the competitiveness of the city and the urban region. The focus is on cities in the neighbouring region (the Nordic countries, Latvia, Lithuania) as well as on the capitals and metropolises of the European Union and partner cities that share cultural and economic interests with Tallinn. The city participates actively in international cooperation projects, especially in the research and development projects financed directly by the European Union, acting as a leading partner in them.

   **Key courses of action:** 1)&nbsp;development of cooperation with the local authorities of Harju County; 2)&nbsp;development of cooperation with cities in the Baltic Sea region, primarily with Helsinki and Riga, the capitals and metropolises of the European Union as well as other partner cities; 3)&nbsp;participation and representation of the interests of Tallinn in the development of the urban policy of the European Union, including through the European urban network EUROCITIES; 4)&nbsp;gaining the international experience required for developing the priority areas of the city and sharing Tallinn's best practices for the purposes of building the international image of the city; and 5)&nbsp;development of a competence centre for international projects.

7. **CIVIL statistics and population services**

   Tallinn is creating a family-friendly environment. The population and integration policy of Tallinn is human-centred and supports the development of various communities. Family and population services are provided comprehensively and professionally, they are accessible to everyone in Estonian, Russian and English both on the premises of the Tallinn Vital Statistics Department and in the International House of Estonia. The expansion of e-services makes the use of these services more convenient. Tallinn contributes to making knowledge-based decisions in matters concerning civil status and raises the awareness of residents. Family and population services are introduced, and advice is given in these issues in cooperation with the private sector.

   **Key courses of action:** 1)&nbsp;vital statistics services; 2)&nbsp;population services; 3)&nbsp;raising the awareness of Tallinn residents about family and population procedures; 4)&nbsp;valuing people who start families and have children; 5)&nbsp;events introducing family values and history; and 6)&nbsp;provision of vital statistics and population services in foreign languages.

8. **Archiving, researching and introducing the history of Tallinn**

   Tallinn takes responsibility and care in the preservation of the city's memory – hundreds of years of archival heritage. The objective of the archival service is to collect, preserve and ensure the usability of information that reflects the development of the Estonian capital, proves the rights and transactions of individuals and is necessary for the work of municipal agencies. In the era of transition to paper-free document management, where the share of digital documents is increasing, the emphasis is on the development of digital archiving and ensuring the respective competency of the city organisation. Tallinn promotes the scientific research and popularisation of the city’s multicultural history by contributing to the strengthening of community identity and historical knowledge. An increasing share of the archive materials of Tallinn is digitally accessible to people all over the world.

   **Key courses of action:** 1)&nbsp;advising the city organisation in the field of archiving and document management; 2)&nbsp;collection and preservation of documents that need to be preserved in municipal agencies; 3)&nbsp;development of digital archiving in accordance with national and international trends; 4)&nbsp;improvement of e-services; 5)&nbsp;organisation of scientific research of the history of the city; and 6)&nbsp;introducing the history of the city.

</Content>
