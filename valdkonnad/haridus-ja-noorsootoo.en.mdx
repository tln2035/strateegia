---
slug: education-and-youth-work
title: Education and youth work
group: valdkond
background: "#0072CE"
---

<Hero>

<RunningHeader>Field of activity</RunningHeader>

## Education and youth work

**The learners in Tallinn are happy.** A happy learner is aware of their strengths, desires and opportunities, which form the foundation for the design of a creative and flexible learning path with diverse choices. The quality of the learning path lays the foundation for the competitiveness of the learner in the future.

The prerequisite for attaining the vision is that learners are responsible. They are supported and empowered in designing and completing the learning path and in developing as a person. A successful individual learning path starts with happy people: competent and dedicated heads of educational and youth work institutions, teachers, youth workers and support specialists as well as families and developers of the education policy. An appropriate cooperation network with an inspiring and balanced learning environment is the base for the development of a happy learner.

</Hero>

<Content>

**This field contributes to a very large extent to the achievement of the strategic goal 'Creative global city' and to a large extent to the achievement of the strategic goals 'Friendly urban space', 'Green transformation' and 'Kind community'.**

**[Creative global city](/en/creative-global-city)** – All action programmes in the field contribute to the development of the open learning space in Tallinn. The diverse and integrated education network of the city, supported by state upper secondary schools and other educational opportunities offered by the state and the private sector, contributes to learning being lifelong and visible in the physical and virtual space of the city. Offering diverse opportunities for learning, self-realisation and discovering creative and development potential helps increase entrepreneurship and creativity. Opportunities for acquiring basic and general education in English support foreigners who have arrived here. The entire learning process supports general digital literacy.

**[Green transformation](/en/green-transformation)** – Integration of the topic of the environment in education and youth work will improve the environmental awareness of citizens: they care about the environment, live by using resources sparingly and consume by the principle of circular economy.

**[Kind community](/en/kind-community))** – Supporting individual development, establishment of a comprehensive network of support and trust for young people at risk of exclusion, good general skills and top-level knowledge contribute to independent and dignified coping. Supporting and encouraging young people to actively participate in social life and decision-making processes contributes to the development of a strong and bold civil society and the integration of society. The development of opportunities to study in Estonian contributes to the preservation of the Estonian language and the integration of society.

**[Friendly urban space](/en/friendly-urban-space)** – Taking young people’s needs into consideration and including them in the creating of urban space supports the development of a friendly urban space.

<!---
<ListHeader>UN sustainable development goals linked to this field</ListHeader>

<ImageGrid maxWidth="88px">

![YRO](../meedia/yro/goal_eng_01.png)

![YRO](../meedia/yro/goal_eng_02.png)

![YRO](../meedia/yro/goal_eng_03.png)

![YRO](../meedia/yro/goal_eng_04.png)

![YRO](../meedia/yro/goal_eng_05.png)

![YRO](../meedia/yro/goal_eng_06.png)

![YRO](../meedia/yro/goal_eng_07.png)

![YRO](../meedia/yro/goal_eng_08.png)

![YRO](../meedia/yro/goal_eng_09.png)

![YRO](../meedia/yro/goal_eng_10.png)

![YRO](../meedia/yro/goal_eng_11.png)

![YRO](../meedia/yro/goal_eng_12.png)

![YRO](../meedia/yro/goal_eng_13.png)

![YRO](../meedia/yro/goal_eng_14.png)

![YRO](../meedia/yro/goal_eng_15.png)

![YRO](../meedia/yro/goal_eng_16.png)

![YRO](../meedia/yro/goal_eng_17.png)

![YRO](../meedia/yro/goal_eng_18.png)

</ImageGrid>
--->

## Principles for implementation of the field

**Responsibility.** Various parties take responsibility for the development and implementation of the individual learning path. Acquisition of education is the task of the learner, but parents, teachers, support specialists, youth workers, supervisors and area managers are also responsible for the provision of education. An individual learning path is developed with the support of a comprehensive responsible network based on the interests, readiness and abilities of the learner. Interaction is achieved through targeted and systematic networking.

**Openness and diversity.** Tallinn is an open and diverse learning space. Institutions that offer formal as well as informal education cooperate in the public and private sectors. Empowered cooperation works between learners, teachers, parents, youth workers, community representatives and other interrelated parties both at the national and international level. Diversity is valued, educational and youth work institutions are unique and various models and methods are used in development activities. Participation of young people in decisions concerning them and developing the skills of young people for making informed choices and decisions and actively participate in social life are considered important. The rights of young people are protected, and active civic participation is supported.

**Innovation.** The educational and youth work institutions of Tallinn implement innovative approaches and a changing learning approach. Innovation is supported at all levels of education and in all educational institutions. The focus is on the knowledge-based and future-oriented use of so-called new skills, including technological literacy, as well as social skills and knowledge. The education provided in Tallinn is competitive and youth work is innovative and considers the actual situation and needs of young people. The experience of informal studies is taken into consideration in formal education and working life.

## Goals of the field

1. **Every learner develops the best in themselves.**

   Learners acquire good general skills and top-level knowledge by making choices based on their strengths, desires and interests.

   <ListHeader>INDICATORS</ListHeader>

   - **The subjective wellbeing (happiness) of learners and the competence and competitiveness of learners.**  
     The starting and target levels will be set in 2021 in order to specify the assessment methodology.
   - **Share of young people who are satisfied with youth work services.**  
     Starting level: the indicator will be reviewed in 2021.  
     Target level: at least 80%. The starting and target levels will be set in 2021 in order to specify the methodology for measuring satisfaction.

2. **Learners have an inspiring and supporting learning environment.**

   A safe and contemporary open space for self-development is guaranteed to learners in all regions of Tallinn.

   <ListHeader>INDICATORS</ListHeader>

   - **Satisfaction of learners with their learning environment.**  
     The starting and target levels will be set in 2021 in order to specify the assessment methodology.

3. **Young people participate actively in social life.**

   Young people are active, proactive and entrepreneurial citizens who participate in the decisions that concern themselves and the entire society.

   <ListHeader>INDICATORS</ListHeader>

   - **Share of candidates aged 18-26 among the candidates in the local government council elections.**  
     Starting level: 8.3% (based on 2017 elections)
     Target level: 15% (2035)

## Action programmes

1. **Top-level management**

   The heads of the educational institutions of Tallinn are motivated and innovative leaders who have good management skills and knowledge of the future directions of education. The organisational culture of the institutions is open and cooperative and supports development. Daily management of education is based on the conscious setting of targets. The heads of education create an environment that supports the individual development and happy learning path of each learner. All educational institutions operate according to agreed values and the head is the person who carries these values. Cooperation and rotation take place between the heads of educational institutions as well as with the private sector. Top specialists recruit managers and develop the next generation. Top-level management also covers the improvement of the management capacity of the Education Department as the agency responsible for the achievement of the field's goals.

   **Key courses of action:** 1)&nbsp;development and training programmes of heads of education; development of the management teams of educational institutions; 2)&nbsp;recruitment of heads of education; monitoring the development of educational institutions; 3)&nbsp;implementation of new management models; 4)&nbsp;improvement of the capacity of the Education Department; 5)&nbsp;motivation and cooperation of heads of education; 6) international cooperation; and 7) bully-free school/nursery school.

2. **Competent and dedicated teacher**

   Dedicated teachers who implement the changing learning approach work in the educational institutions of Tallinn. Teachers use modern teaching methods and inspire students. The next generation of teachers is guaranteed in all areas and their work is valued. Teachers have time for self-education and cooperation. The homeroom teacher plays a central role in supporting the development of learners.

   **Key courses of action:** 1)&nbsp;contemporary teacher training; 2)&nbsp;valuing the teaching profession; 3)&nbsp;recruitment of new teachers; and 4)&nbsp;designing the organisation of the study process; empowerment of the institution of homeroom teacher.

3. **Development of students with special educational needs**

   Suitable educational opportunities have been created for all students with special educational needs. Tallinn has an integrated and competent support network. Children with special educational needs are provided with the necessary support at every level of education. A centre of excellence has been developed that supports educational institutions in dealing with children with special educational needs and implementing inclusive education. The next generation of motivated and professional support specialists is ensured. There are specialists working in educational institutions who are able to implement inclusive education. Gifted children are systematically developed.

   **Key courses of action:** 1)&nbsp;educational opportunities for students with special educational needs; 2)&nbsp;implementation of a comprehensive system of support services; 3)&nbsp;development of support specialists; 4) improvement of the capacity of educational institutions in respect of special educational needs; and 5)&nbsp;programme for gifted students.

4. **Inspiring and innovative learning**

   The educational institutions of Tallinn consistently implement knowledge-based innovations and carefully develop new and future skills. Area-specific centres of excellence, which are open to different educational institutions, have been developed. Success stories and good examples are available to everyone. Institutions have the tools that support the changing learning approach. Natural and exact sciences and entrepreneurship are developed, new learning and teaching models are designed and implemented.

   **Key courses of action:** 1)&nbsp;knowledge-based innovations and pilot projects; 2)&nbsp;development of comptetence centres; 3) updating teaching materials and technological applications; 4)&nbsp;development of natural and exact sciences and entrepreneurship; and 5)&nbsp;designing the study process, new learning and teaching models.

5. **Individual learning path**

   An individual learning path model, which covers formal and informal education and vocational and higher education, is implemented in Tallinn. Learners are guaranteed professional support in designing their individual learning paths and making choices. Education information is in a common system that supports all learners. Opportunities for learning in Estonian have been systematically developed. Parents and the community cooperate efficiently.

   **Key courses of action:** 1)&nbsp;designing an individual learning path; 2)&nbsp;developing a common information space in the field of education; 3)&nbsp;developing cooperation with parents and the community; and 4) developing possibilities for learning in Estonian.

6. **Balanced and diverse education network**

   Tallinn has a diverse and integrated education network; good basic education is available close to home. The city responds flexibly and quickly to changes in demand and finds solutions for all learners. State upper secondary schools and other educational opportunities offered by the state and the private sector diversify the city's education network. Learning environments are safe and modern and support the implementation of the changing learning approach. It is possible to acquire basic and general education in English in the city.

   **Key courses of action:** 1)&nbsp;comprehensive planning of the education network; 2)&nbsp;modernisation of educational institutions; 3)&nbsp;creation of opportunities for international education; and 4)&nbsp;designing the space for open learning.

7. **Modern and developing youth work**

   Young people participate actively in social life, their skills allow them to cope in a rapidly changing world. Different parties are aware of how to include young people efficiently at the earliest age possible and at different levels.

   Youth work focuses on young people and their interests and needs. Contemporary and diverse youth work services enrich and support the daily lives of young people and create opportunities for unlocking and discovering their potential. The youth work service is suitable for the age and abilities of young people, takes into consideration the diverse needs and interests of young people, creates equal opportunities, promotes an environmentally friendly mindset, ensures the development of social skills and helps reduce the risk of social exclusion. Youth work offers good opportunities for experimenting, succeeding as well as making mistakes and emphasises the opportunity to learn from experience. Youth work also contributes to lifelong learning, develops future work skills and contributes to the consideration of the results of informal studies in the curricula of formal education and working life.

   Equipment and infrastructure that are modern and meet the expectations and needs of young people are used to provide youth work services. The active environments of youth work institutions and the urban space are diverse and safe; they support learning and help prevent the risk-taking behaviour of young people. The specialists working in the field of youth work are valued and professional.

   **Key courses of action:** 1)&nbsp;empowerment and inclusion of young people in decisions; 2)&nbsp;contemporary youth work services that develop young people; 3)&nbsp;staff, infrastructure and resources that support quality youth work; and 4)&nbsp;noticing and supporting young people with limited opportunities.

</Content>
