---
slug: mobility
title: Mobility
group: valdkond
background: "#0072CE"
---

<Hero>

<RunningHeader>Field of activity</RunningHeader>

## Mobility

**Healthy mobility.** Mobility opportunities that support health and are accessible and convenient are guaranteed throughout the urban region of Tallinn. A pleasant environment for walking and cycling allows people to perform their everyday activities in fresh air. Fast transport routes have been established in the main traffic corridors with big capacities and convenient connection points, where people can continue their journeys in smaller buses and use the last-mile <Annotation id="micromobility">micromobility services&nbsp;</Annotation>. The provision of these services significantly improves the user-friendliness of public transport in Tallinn. Calm and safe car traffic makes it possible to travel without major obstacles. A well-designed urban space and balanced mobility opportunities turn Tallinn into a green and creative global city with healthy people.

</Hero>

<Content>

**This field contributes to a very large extent to the achievement of the strategic goals 'Healthy mobility' and 'Friendly urban space' and to a large extent to the achievement of the strategic goals 'Home that includes the street', 'Green transformation' and 'Creative global city'.**

**[Healthy mobility](/en/healthy-mobility)** – Increasing the share of active types of mobility in city traffic improves people's health. Good connections and the implementation of universal design principles ensure that everyone can access their destinations.

**[Friendly urban space](/en/friendly-urban-space)** – Increasing the share of sustainable types of mobility, safer traffic and the balanced division of street space as well as well-designed streets make for a human scale urban environment. The connection centres of public transport will be designed as parts of quality urban space.

**[Green transformation](/en/green-transformation)** – Increasing the share of sustainable types of mobility and switching to alternative fuels reduces CO₂ emissions and stress on the natural environment.

**[Home that includes the street](/en/home-that-includes-the-street)** – Reducing parking and speeds in calm traffic areas and promoting environmentally friendly types of mobility makes for more attractive space and cleaner air. Good public transport and light traffic connections increase the value of homes.

**[Creative global city](/en/creative-global-city)** – Good intra-city and regional connections promote entrepreneurship and good international connections improve the competitiveness of Tallinn.

<!---
<ListHeader>UN sustainable development goals linked to this field</ListHeader>

<ImageGrid maxWidth="88px">

![YRO](../meedia/yro/goal_eng_01.png)

![YRO](../meedia/yro/goal_eng_02.png)

![YRO](../meedia/yro/goal_eng_03.png)

![YRO](../meedia/yro/goal_eng_04.png)

![YRO](../meedia/yro/goal_eng_05.png)

![YRO](../meedia/yro/goal_eng_06.png)

![YRO](../meedia/yro/goal_eng_07.png)

![YRO](../meedia/yro/goal_eng_08.png)

![YRO](../meedia/yro/goal_eng_09.png)

![YRO](../meedia/yro/goal_eng_10.png)

![YRO](../meedia/yro/goal_eng_11.png)

![YRO](../meedia/yro/goal_eng_12.png)

![YRO](../meedia/yro/goal_eng_13.png)

![YRO](../meedia/yro/goal_eng_14.png)

![YRO](../meedia/yro/goal_eng_15.png)

![YRO](../meedia/yro/goal_eng_16.png)

![YRO](../meedia/yro/goal_eng_17.png)

![YRO](../meedia/yro/goal_eng_18.png)

</ImageGrid>
--->

## Principles for implementation of the field

&VeryThinSpace;<Annotation id="organisation-of-mobility">Organisation of mobility</Annotation> **before investments.**

Before large-scale investments are made, mobility management measures are implemented for the improvement of mobility options (e.g. new developments that require the movement of large numbers of people are planned in the proximity of well-functioning public transport or developed with new public transport connections). Streets are planned according to what type of mobility should be increased.

## Goals of the field

1. **The division of types of mobility in the Tallinn region is balanced.**

   Urban space and well organised public transport promote active and sustainable mobility – walking, cycling and public transport. Mobility infrastructure – city streets, public transport, cycle lanes, health trails, squares and parks – is attractive. As a result, everyone can choose the most suitable means depending on their journey: short distances by walking and cycling, longer ones between centres by public transport and journeys outside centres by combining different types of mobility or by car. A journey by public transport takes no more than half an hour on average or no longer than 20 minutes between city centres and other <Annotation id="centre">centres</Annotation>. Walkability is supported by mobility services at a new level. They are affordable, convenient, reliable and environmentally friendly and allow people to move between the main residential, working and study areas of Tallinn within a reasonable time. Most of the homes, jobs, city squares and parks are located in areas with very good public transport connections, i.e. the stops are not farther than 400 metres away.

   <ListHeader>INDICATORS</ListHeader>

   - **Share of movement by public transport, on foot or by bicycle in the Tallinn urban region.**  
     Starting level: will be set in 2021
     Target level: 50% (2025), 70% (2035)
   - **Change in greenhouse gas emissions from transport in Tallinn in comparison with 2007**  
     Starting level: -1% (2015)  
     Target level: -40% (2030)
   - **Average duration of trip between centres by public transport**  
     Starting level: will be set in 2021  
     Target level: will be set in 2021

2. **The mobility infrastructure of the Tallinn region is accessible, and the destinations are well connected for all types of mobility.**

   The fact that streets, pavements, public transport stops and the main network of bicycle lanes are accessible to everyone at all times throughout their lives contributes to integral and accessible journeys. Accessible means that all people, including those using wheelchairs or guide dogs or are out with pushchairs or prams, can use the infrastructure, especially the streets and public transport, independently. Tactile surfacesfor visually impaired people are used on pavements and pedestrian crossings and smart city solutions for ease of movement are also used on the main roads. All new and reconstructed city streets comply with accessibility requirements and the biggest obstacles to movement will be removed from the existing streets. All public transport vehicles, public transport stops and hubs are also accessible to pushchair, pram and wheelchair users.

   <ListHeader>INDICATORS</ListHeader>

   - **Share of people who are very satisfied with the accessibility of the main network of streets, pavements, public transport stops and bicycle lanes all year round.**  
     Starting level: will be set based on 2020 data  
     Target level: will be set after the starting level has been determined
   - **Share of students of general education schools who go to school, hobby schools and training facilities independently.**  
     Starting level: 87% (schools, 2019), 86% (hobby schools and training facilities, 2019)  
     Target level: 90% (2035)

3. **The mobility environment of the Tallinn region is safe.**

   The principle Tallinn follows in the organisation of mobility is that nobody should be killed in traffic. Most road users feel that the mobility environment in Tallinn is safe, the number of accidents and traffic losses are decreasing. A pleasantly designed urban space and good organisation of traffic reduce the number of cases of speeding and careless attitudes towards other road users and allows pedestrians and cyclists to move comfortably as well. Roads and pavements that are in good order and maintained all year round make movement safer. People who use different means of transport (including pedestrians) are clearly separated from one another so that pedestrians and cyclists can also move safely and smoothly without being forced to move from one side of the road to the other, and safety on smaller streets is guaranteed with traffic calming measures and speed limits.

   <ListHeader>INDICATORS</ListHeader>

   - **The number of traffic accidents with human victims decreases by half.**  
     Starting level: 505 (2019)  
     Target level: <253 (2035)
   - **Share of residents who feel that the mobility environment in Tallinn is safe.**  
     Starting level: will be set based on the citizens satisfaction survey of 2020  
     Target level: will be set after the starting level has been determined

## Action programmes

1. **Complete and safe street space**

   The street space has been designed and is maintained in a manner that enables all types of mobility and ensures access for all road users. The principle of nine street types (see the scheme), which takes into consideration the nature and importance of the locality, is thereby followed. The most important goal is to improve the quality and safety of the existing street network, especially pavements and bicycle lanes, while the street network has become denser. There is enough greenery in the city, which reduces the impact on the transport system by extreme weather conditions, floods caused by stormwater and heat islands. The design of streets meets the requirements of universal design and calms traffic. The condition and level of maintenance of the streets largely correspond to requirements. The main network of bicycle lanes has been built by 2035.

   **Key courses of action:** 1) comprehensive planning of streets and roads considering various types of mobility, reconstruction of streets and construction of new streets proceeding from the principle of nine street types; 2) maintenance that considers the locality value of the streets and the preferred types of mobility; 3) uniform planning of mobility solutions for new residential areas, schools and workplaces, centres and social infrastructure; 4) purchase of land in places strategically important from the point of view of balanced mobility; and 5) creation of a safe mobility environment.

   Principles of nine street types in Tallinn

   ![](../meedia/valdkonnad/liikuvus_eng.svg)

2. **Fast and convenient public transport**

   Public transport is a part of good urban space. It is a pleasant experience for passengers and gives most people the chance to be free of the obligations and expenses related to cars. Instead of a personal car, the people of Tallinn prefer high-speed public transport connections, which are the backbone of city traffic, for travelling between the centres of the city. The average journey between the city centre and other centres of the city by public transport usually doesn't last more than 20 minutes. There are convenient and clearly marked connection possibilities between the main routes (including railway). The user experience offered by public transport (service, indoor climate, cleanliness, design, light) is pleasant in all seasons and the information about schedules is communicated in real time, as a result of which people are increasingly more satisfied with the convenience of public transport. Public transport corresponds to the principles of universal design. In cooperation with neighbouring local governments and the state, the city has created a single regional fare system and route network. The travel cards of the Tallinn region can be used in other cities of Estonia and Helsinki and vice versa. Most homes and jobs are located in excellent public transport service areas. This means that the public transport stops are not farther than 400 m from most homes, jobs or city squares and parks. Public transport stops in centres comply to requirements of universal design by 2030. An updated public transport route network will be proposed by 2023, which is based on the mobility analyses of residents and corresponds to the changes that have taken place in the urban space.

   **Key courses of action:** 1)&nbsp;planning and development of a new route network (including development of a service level standard); 2)&nbsp;planning of convenient connection points of public transport for connecting various types of mobility (including trains, city and county transport, 'Park & Ride' system) and their development; 3)&nbsp;reduction of the connection times required for mobility; 4)&nbsp;improvement of the accessibility of public transport according to universal design; 5)&nbsp;making public transport vehicles attractive (lighting, cleanliness, suitable inner climate); and 6) making public transport stops user-centred and weatherproof, installation of real-time information systems at public transport stops.

3. **Convenient cycling**

   Use of all <Annotation id="micromobility-vehicle">micromobility vehicles</Annotation> is considered equal to the use of bicycles in this action programme, as the same infrastructure is planned for both of them. Travelling by bicycle in the city is convenient, fast and safe. Bicycles are suitable for all daily journeys all year round: for going to work or school and for spending leisure time. Bicycles are mainly used for short and medium length trips (up to 5 km). At least 25% of school-goers use bicycles, as the routes to schools are safe and well maintained. All social groups use bicycles (the share of bicycle trips among all trips is at least 11%). The action programme will be implemented within the scope of the Tallinn Bicycle Strategy 2018-2027.

   **Key courses of action:** 1)&nbsp;construction of a comprehensive core network of bicycle lanes will continue (it will be carried out with the first action programme); 2)&nbsp;design of cycle-friendly and safe roads to school; 3)&nbsp;building bicycle parking in all regions and centres, supporting the building of bike parking near homes and schools; 4)&nbsp;the construction of a complete health network will continue; 5)&nbsp;keeping bicycle lanes usable all year round; and 6)&nbsp;development of cycling culture and promotion of safe traffic.

4. **New technology and services**

   The new mobility and goods transport services make combining modes of transport more convenient, faster and safer and reduce dependence on personal cars. Tallinn adapts to the new mobility services on the market (e.g. taxis, ridesharing, rental cars) and integrates them into the existing transport system and environment by cooperating to make them substantive parts of the public transport system. The focus is on the movement and accessibility of people and goods instead of the movement of vehicles. The share of people who use mobility services increases. The present vehicles will be replaced with electric and hydrogen vehicles hand in hand with the development of technology. The city's vehicle fleet has transferred to vehicles operating on gas in the first phase (diesel-free buses by 2025) and to electric and/or hydrogen vehicles in the second phase (free of fossil fuels by 2035). Taxis, rental transport and intra-city transportation of goods have transferred to emission-free vehicles to the extent of 50% by 2030.

   **Key courses of action:** 1)&nbsp;establishment of cycling and micromobility circulation systems in cooperation with the private sector; 2)&nbsp;creating places in centres where users of car pooling and taxi services can embark and disembark; 3)&nbsp;integration of the short-term rental system of vehicles into the Tallinn mobility environment and organisation thereof in cooperation with the private sector; 4)&nbsp;implementation of the last-mile service via the testing of autonomous vehicles; 5)&nbsp;expanding the electric vehicle charging system in cooperation with the private sector; 6)&nbsp;organisation of travel planning services combining various types of mobility in cooperation with the private sector; 7)&nbsp;introduction of sustainable transport and mobility in public sector institutions and private companies; 8) increasing the share of environmentally sustainable public transport vehicles and developing infrastructure that supports this.

5. **Regional and international mobility**

   Tallinn forms an integral whole with nearby municipalities, where combining various types of mobility and public transport services is convenient and well organised. The creation of a common route network and the uniform organisation of public transport makes it possible to use money more efficiently and reduces the share of people who drive from neighbouring municipalities to Tallinn. The regional organisation of public transport (including a single ticketing system covering all types of public transport) has been agreed on at the level of the city, region and state. The volume of transit freight transport through the city centre has decreased. The city and the state join forces to work on the maintenance and development of flight and ferry connections. Tallinn City Government is an active participant in the planning of the Tallinn-Helsinki tunnel in order to establish a permanent connection between the twin capital of Tallinn and Helsinki.

   **Key courses of action:** 1)&nbsp;planning of public transport types and ordering of services in the urban region will be concentrated in a joint mobility organisation in cooperation with the state and neighbouring local authorities; 2)&nbsp;development of an integrated route network and convenient connection possibilities in the capital region; 3)&nbsp;transition to a single zone-based fare system; 4)&nbsp;improved access to public transport (especially rail transport) in the urban region, bringing bicycle network nodes and bicycle parking to public transport stops; 5)&nbsp;establishment of 'Park & Ride' car parks near public transport stops, train stations and other hubs on the way to Tallinn; 6)&nbsp;development of cooperation between Tallinn-Helsinki and the state, including redirection of the transport of goods from the ports out of the city; 7)&nbsp;agreeing on the financing of regional public transport; and 8) development international associations and participation in cooperation projects.

6. **Parking suitable for the city environment**

   Parking will be designed according to the specifics of each region. New principles and standards have been established for the organisation of parking that support the achievement of the objectives of mobility. The parking standard is, among others, based on the level of public transport connection. In the residential areas of Mustamäe, Lasnamäe and Õismäe, people park their cars in the parking garages on the outer border of the areas, where possible. No concessions for parking in the streets are made to the residents of new developments. Parking in new residential areas is generally organised in underground car parks or parking garages. Outdoor car parks are green and water-permeable in areas where this is geologically possible.

   **Key courses of action:** 1)&nbsp;updating the parking principles and standards; 2)&nbsp;establishment of parking infrastructure; and 3)&nbsp;development of parking charges.

7. **Sustainable financing**

   The financing of the transport system is sustainable and supports all types of mobility. The organisation and financing of the transport system between different types of transport, administrative levels and authorities is coordinated. Cooperation models for involving the private sector in financing the infrastructure have been developed. The focus is on improving the quality of the public transport infrastructure and public space, traffic safety, development of bicycle lanes and pavements and the level of maintenance of road infrastructure. Big public transport investments are socio-economically profitable.

   **Key courses of action:** 1)&nbsp;devising a co-funding model for the development of regional public transport, infrastructure and mobility services; and 2)&nbsp;involvement of the private sector in financing the infrastructure.

8. **Traffic control and planning**

   Traffic in Tallinn is smooth, problem-free and safe and causes minimal damage to the environment and all types of mobility have been taken into consideration. Traffic management corresponds to the nature and function of street types. Traffic is planned by common principles to create a mobility environment that is perceived the same everywhere according to the value of the place.

   **Key courses of action:** 1)&nbsp;development of the Tallinn mobility model; 2)&nbsp;development of an adaptive traffic control system; 3)&nbsp;expansion of the public transport priority system; 4)&nbsp;establishment of a green corridor for traffic flows of main demand; and 5)&nbsp;expansion of the vehicle monitoring system.

</Content>
