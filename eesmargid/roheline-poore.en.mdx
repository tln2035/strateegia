---
group: üldeesmärk
running_title: STRATEGIC GOAL4
title: Green transformation
slug: green-transformation
heroimage: "hero_roheline"
menuimage: "menyy_roheline"
metaimage: "meta_roheline"
image_alt: ""
background: "#85cd9f"
hover: "#009639"
---

<Hero>

<RunningHeader>STRATEGIC GOAL 4</RunningHeader>

# Green transformation

![Green transformation](../meedia/eesmargid/hero_roheline.svg)

Sustainability and the green transformation do not concern the natural environment of Tallinn alone, but are a central principle of the entire society and economy. The way of life of the people living in Tallinn is environmentally friendly because they feel responsible for the future: every generation must create and maintain a good living environment for their children. Tallinn will adapt to climate change and reduce greenhouse gas emissions to climate neutrality by the middle of the century. There is clean air and water in the city and the biodiversity of urban nature is protected. The citizens care about the environment, live in a resource-efficient manner and consume according to the principle of circular economy. The knowledge, attitudes and behaviour of the citizens turn Tallinn into a pioneering city with a good living environment.

<ReadMore id="annex-4-1" />

<LightBox previewImageUrl="https://img.youtube.com/vi/HCYbvip3Ddo/hqdefault.jpg">
  <iframe
    width="100%"
    src="https://www.youtube.com/embed/HCYbvip3Ddo?autoplay=1&yt:cc=on&hl=en&cc_lang_pref=en&cc_load_policy=1"
    frameborder="0"
    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
    allowfullscreen
  ></iframe>
</LightBox>

</Hero>

<Content>

## Climate-neutral city

<MapTrigger id="S42-pargid" isDefault={true}>

In each of its decisions, Tallinn follows the objective of climate neutrality, reducing carbon emissions by 40% by 2030 (compared with 2007), and will be a climate-neutral capital by 2050. The City of Tallinn is planned in consideration of carbon neutrality. Mobility functions on non-fossil fuels. Buildings are resource-efficient and climate-neutral energy is used in them. Clean energy is used in production. Tallinn adapts to climate change and extreme weather conditions using nature-based solutions and making buildings and infrastructure more climate-resistant. Climate neutrality is regarded as a cross-sectoral topic, which is specified in the development document on sustainable energy economy and climate.

<!-- **Measures to Reduce Carbon Emissions**

1. **Electric and hydrogen vehicles**&nbsp;(-179kt/a)
2. **Light traffic and urban planning that favours it**&nbsp;(-127kt/a)
3. **Integrated public transport**&nbsp;(-99kt/a)
4. **Taxing the use of polluting vehicles in the city**&nbsp;(-99kt/a)
5. **A higher parking fee for polluting vehicles**&nbsp;(-70kt/a)
6. **The innovative establishment and renovation of buildings**&nbsp;(-42kt/a)
7. **The use of biogas in public transport**&nbsp;(-34kt/a)
8. **Fostering and promoting sustainable traffic**&nbsp;(-28kt/a)
9. **Light traffic roads and their year-round maintenance by robots**&nbsp;(-22kt/a)
10. **Building-based clean energy solutions**&nbsp;(-13kt/a)
11. **Carsharing and car rent**&nbsp;(-12kt/a)
12. **District cooling solutions**&nbsp;(-12kt/a)

<Meter>


![](../meedia/moodikud/saastupuu.svg)

</Meter> -->

<ReadMore id="annex-4-2" />

</MapTrigger>

## Fertile ground for green innovation

<ImageTrigger id="4_roheuuendused.jpg" alt="">

Green transformation means a change in the mindset and values of the society as a whole. Tallinn has a good environment in which companies, non-governmental organisations, government organisations and residents can launch green innovations. Green innovations may be technological and process-related, organisational and social – it is possible to reduce environmental impact and improve wellbeing with a variety of initiatives. The people of Tallinn act responsibly towards the future and find solutions to the challenges of green transformation together by cooperating with the leaders of this sector and research institutions throughout the world.

<ReadMore id="annex-4-3" />

</ImageTrigger>

## Biodiverse and blossoming urban nature

<MapTrigger id="S43-imbala">

Tallinn stands out among European capitals with its clean air and species richness thanks to its biodiverse nature and extensive green network. The cohesive green network – parks, forests and private gardens – increases the biodiversity and offers the city people an exciting natural environment, leisure opportunities and protection against extreme weather conditions. Plants are allowed to blossom. The urban space is designed using green material that is more suitable for biodiversity, especially for pollinators. Overburdening natural biotic communities is avoided in order to give them the chance to recover themselves. Stormwater is treated using innovative and natural solutions so that it can be used for the needs of citizens as well as for increasing the viability of urban nature. Seawater and all springs, rivers and lakes of Tallinn are in an ecologically good condition. We improve the health of citizens and food safety by increasing natural biodiversity.

<Meter title="By 2035">

**Natural surfaces** will make up at least **65%** of Tallinn’s area

![Illustratsioon](../meedia/moodikud/S26.svg)

<MeterData>

2020: **67%**

</MeterData>

</Meter>

<ReadMore id="annex-4-4" />

</MapTrigger>

## Circular economy

<ImageTrigger id="4_ressursis22stlik.jpg" alt="">

Tallinn has transferred to <Annotation id="circular-economy">circular economy</Annotation>. Less waste is generated in the city as a result of more informed production and consumption: the majority of municipal waste is recycled. Among others, less food is thrown away and the city's food circulation system is well thought out. Resource-efficient industrial enterprises use materials sustainably and take responsibility for re-processing their products. The life cycle of each product and service and their recovery have been thought through in consideration of environmental impact. Environmentally friendly materials, which can be recycled well, are produced and consumed in Tallinn. Waste is collected in an environmentally and consumer friendly manner. Collection of waste by type is organised in the places where waste is generated (including in the public space). As a maritime city, Tallinn gives a lot of attention to reducing marine debris.

<Meter title="By 2035">

The share of materials in circulation will **increase**

![Illustratsioon](../meedia/moodikud/S27.svg)

<MeterData>

2017: **8,7%**

</MeterData>

</Meter>

<ReadMore id="annex-4-5" />

</ImageTrigger>

</Content>

<DomainsGrid>

## Key fields

<Domain id="environmental-protection">
  In order to conserve natural resources, Tallinn is moving towards developing a
  circular economy by minimising the use of mineral resources for production
  with the help of the entire society and recycling materials as much as
  possible. We collect and manage data on the natural environment and work out
  guidelines in order to maintain the biodiversity and diverse urban nature. An
  increased environmental awareness will encourage people to act sustainably.
</Domain>

<Domain id="urban-planning">
  We are fostering climate neutrality, a healthy environment that is full of
  life and the achievement of resource efficiency by shaping a compact and
  varied urban space that enables the use of various modes of transport.
  Heritage board helps to maintain the historically valued urban space for the
  generations to come.
</Domain>

<Domain id="mobility">
  Increasing the share of sustainable modes of transport and transitioning to
  alternative fuels will decrease CO₂ emissions and conserve the environment.
  Reducing aboveground parking and speed limits in calm traffic areas and
  encouraging attractive and environmentally friendly modes of transport will
  make calm traffic areas more attractive and the air cleaner. Good public
  transport and light motor road connections increase the value of homes.
</Domain>

<Domain id="city-property">
  Designing new optimal spatial planning and energy efficient urban buildings
  and contributing to making homes in Tallinn more energy efficient supports
  climate neutrality and resource efficiency.
</Domain>

## Contributing fields

<Domain id="business-environment">
  Using innovative and smart solutions promotes eco-innovation by helping to
  alleviate the climate impact, adapting to climate change and creating a good
  living environment. Developing balanced and sustainable tourism contributes to
  making our ecological footprint as small as possible.
</Domain>

<Domain id="education-and-youth-work">
  Integrating environmental topics into education increases the environmental
  awareness of city residents: they care more about the environment, have more
  resource efficient lifestyles and consume based on the principles of circle
  economy. We intertwine the promotion of environmentally sustainable ways of
  thinking into youth activities, including by carrying out specific
  environmental-themed projects.
</Domain>

<Domain id="municipal-order">
  Prevention (including raising people’s awareness) and supervision of following
  the rules will help to create a safe and healthy environment.
</Domain>

<Domain id="urban-landscape">
  A diverse landscape supports the survival of biodiversity. A connected green
  network allows people to choose sustainable ways of living and getting around.
  The smart handling of the entire lifecycle enables us to plan landscaping
  better. Community gardens increase residents’ environmental knowledge,
  strengthen connections between people and enable local production of food with
  a minimal carbon footprint. We take into account all the residents’ needs in
  the urban landscape which makes the city more attractive.
</Domain>

<Domain id="utility-networks">
  Sustainable and cost-effective solutions and minimising the environmental
  damages when developing utility networks contributes to saving energy and
  maintaining the natural environment.
</Domain>

</DomainsGrid>
