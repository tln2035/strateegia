---
group: üldeesmärk
running_title: STRATEGIC GOAL 3
title: Healthy mobility
slug: healthy-mobility
heroimage: "hero_liikuv_terve_tln"
menuimage: "menyy_liikuv_terve_tln"
metaimage: "meta_liikuv_terve_tln"
image_alt: ""
background: "#f79da3"
hover: "#f25c66"
---

<Hero>

<RunningHeader>STRATEGIC GOAL 3</RunningHeader>

# Healthy mobility

![Healthy mobility](../meedia/eesmargid/hero_liikuv_terve_tln.svg)

The living environment in Tallinn supports people's good health. Exercising and spending time outdoors is pleasant irrespective of the weather. Most people use fast and accessible public transport, walking or cycling to get around in Tallinn. Mobility has become a user-friendly service that makes it possible to combine different forms of mobility conveniently and economically. Attractive public transport vehicles and weather-proof bus and tram shelters are a natural part of the urban space. Schoolchildren and the elderly, wheelchair users and people with prams or guide dogs can all move without assistance in Tallinn. There are also fast public transport connections in the urban region and people use them more and more. There is considerably less car traffic, it causes less pollution, is calm and free of traffic jams.

<ReadMore id="annex-3-1" />

<LightBox previewImageUrl="https://img.youtube.com/vi/U9lpiGXvtQQ/hqdefault.jpg">
  <iframe
    width="100%"
    src="https://www.youtube.com/embed/U9lpiGXvtQQ?autoplay=1&yt:cc=on&hl=en&cc_lang_pref=en&cc_load_policy=1"
    frameborder="0"
    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
    allowfullscreen
  ></iframe>
</LightBox>

</Hero>

<Content>

## Healthy lifestyles

<ImageTrigger id="3_tallinlased.jpg" isDefault={true} alt="">

The environment of Tallinn supports the good mental and physical health of its citizens. It is possible to lead an active life and eat healthy in Tallinn; people's lives are not ruled by alcohol, tobacco and other addictive substances. The working and private lives of the residents of Tallinn are balanced and the time spent with their families and friends is valued. The number of deaths caused by cardiovascular diseases and malignant tumours has decreased. Citizens of all ages enjoy spending their free time outdoors and being active. A pleasant urban space, the possibility to recharge your batteries in nature, communities doing things together, better health awareness and active mobility opportunities help reduce stress and depression and prevent obesity. The foundation of good health behaviour is laid by good exercise and healthy habits acquired during childhood. The number of accidents decreases every year.

<Meter title="By 2035">

Healthy life expectancy: women&nbsp;–&nbsp;**63**, men&nbsp;–&nbsp;**62**

![Illustration of healthy life years](../meedia/moodikud/S41.svg)

<MeterData>

2018: **58.4**&nbsp;women, **57.4**&nbsp;men

</MeterData>

</Meter>

<ReadMore id="annex-3-2" />

</ImageTrigger>

## Mobility service at a new level

<MapTrigger id="S13-ut_uhendused">

Mobility is at a new level in Tallinn as well as in the entire urban region. The good <Annotation id="mobility_service">mobility service</Annotation> offered allows most citizens to get around easily without a car. Urban traffic is calm and safe, there are fewer traffic jams and less noise in the streets. Fast tram connections are supported by buses, bike sharing, short-term vehicle rental and ride sharing. The city is a global pioneer in the implementation of self-driving technology, integrating it into a complete mobility system. The use of various means of mobility is fast, affordable, comfortable, reliable and environmentally sustainable. The majority of the housing and jobs in the urban region lay in the service area of good public transport, where stops are at most 400 metres away and people don't have to wait long for vehicles to arrive.

<ReadMore id="annex-3-3" />

</MapTrigger>

## Life in fresh air

<MapTrigger id="S14-rattateede-vork">

The urban space of Tallinn favours active forms of mobility. A clean, noise free and pollution free city environment supports a mobile lifestyle. The living environment in the city has been designed so that nurseries, schools, shops and leisure facilities are within walking distance for citizens. Tallinners prefer moving around the city year-round. Active mobility is supported by the street space, secure bicycle parking and the green network connecting the entire city. City streets, public transport, cycle lanes, health trails, squares and parks are safe and attractive. People can move and exercise near their homes or workplaces – e.g. at the local school, sports field, sports club or city space.

<ReadMore id="annex-3-4" />

</MapTrigger>

## City accessible to everyone

<MapTrigger id="S15-ut-ligipaas">

The city environment is suitable for everyone, including those who move around with mobility aids and prams, children and the elderly. Tallinn proceeds from <Annotation id="universal_design">universal design</Annotation> in all developments and reconstruction. Public buildings, housing and workplaces are accessible to everyone and using pavements, intersections and public transport stops is easy for everyone. Streets and parks hold enough benches with back rests and shelters that can be accessed by wheelchair or with prams and where people can also work remotely. In addition to the physical space, the virtual space, i.e. the information systems and services that are provided electronically, will also be designed accessible to everyone.

<Meter title="By 2035">

**90% of residents** will have a public transport stop within a **400 metre radius of their homes**

![Illustration of public transport accessibility](../meedia/moodikud/S16.svg)

<MeterData>

2020: **87,3%**

</MeterData>

</Meter>

<ReadMore id="annex-3-5" />

</MapTrigger>

</Content>

<DomainsGrid>

## Key fields

<Domain id="mobility">
  Increasing the share of active types of movements improves people’s health.
  Implementing the principles of good connection options and universal design
  allows everyone to access any destination. A fast and comfortable public
  transport system frees city residents from the obligation of owning a car and
  helps them avoid traffic jams. Interconnected mobility services allow everyone
  to find an optimal path for them when travelling in the city.
</Domain>

<Domain id="urban-planning">
  Implementing the planning principles of a compact and human-sized space helps
  to reduce long unnecessary commutes and fosters active types of movement and
  the use of public transport. The design of the urban space creates an
  environment that is accessible to everyone. Our plans include attractive spots
  where everyone can spend time in the fresh air.
</Domain>

<Domain id="health-and-healthcare">
  Fostering healthy lifestyles and ensuring the accessibility of health care
  prolongs the time during which people live healthily, contributes to the
  intertwined society, independent coping and a strong sense of security as well
  as increases employment.
</Domain>

<Domain id="sports-and-physical-activity">
  Developing sports infrastructure and services and raising awareness about the
  benefits of an active lifestyle will help Tallinn to become a city with active
  ways of getting around and Tallinn residents will be healthy and live a long
  life.
</Domain>

## Contributing fields

<Domain id="environmental-protection">
  A well-maintained natural environment improves people’s well-being and impacts
  both their mental and physical health positively. Cleaner air and less noise
  promotes spending time in the fresh air as well as a healthier and stress-free
  way of movement.
</Domain>

<Domain id="urban-landscape">
  The diverse landscape and playgrounds make the urban space attractive, thus
  they will foster spending time and walking around in the fresh air. Tallinn’s
  blue and green network establishes places where city residents can spend their
  time and move around healthily. The cohesion of the green spaces will allow
  people to travel almost from one end of the city to the other by bike or on
  foot so that their contact with car traffic will be minimal. A well-maintained
  and diverse landscape will make the street space more pleasant.
</Domain>

<Domain id="city-property">
  Universal design principles will be implemented to shape the real estate
  environment and improve the accessibility of the urban space. Owning the
  necessary land or rights for development in the urban space enables us to
  develop mobility services at a new level.
</Domain>

<Domain id="social-welfare">
  By supporting independent coping and offering social welfare aids, it is
  ensured that people are both mentally and physically healthy and live longer.
  The urban space will have to be accessible also to those who need aids to move
  around.
</Domain>

<Domain id="utility-networks">
  A compact utility network will help with shaping landscaping and other
  aboveground infrastructure and developing an attractive urban space. Moreover,
  well-thought out street lighting will help to guarantee everyone’s safety.
</Domain>

</DomainsGrid>
