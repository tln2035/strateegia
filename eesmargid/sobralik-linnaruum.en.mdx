---
group: üldeesmärk
running_title: STRATEGIC GOAL 1
title: Friendly urban space
slug: friendly-urban-space
heroimage: "hero_linnaruum"
menuimage: "menyy_linnaruum"
metaimage: "meta_linnaruum"
image_alt: ""
background: "#84BBE8"
hover: "#0072CE"
---

<Hero>

<RunningHeader>STRATEGIC GOAL 1</RunningHeader>

# Friendly urban space

![Friendly urban space](../meedia/eesmargid/hero_linnaruum.svg)

Tallinn is a human scale city close to nature and accessible to everyone and has a compact city centre and diverse district centres. There are many city squares, parks, cafés, small shops and other places that have been designed to be comfortable for people. Viable green and water areas enrich the quality urban space. The open seaside offers various opportunities for activities. The urban space, transport and buildings are all easily accessible to everyone. Being outside alone is safe for everyone, including children and the elderly. People walk and use various micromobility vehicles and everyone's safety is guaranteed. Public transport increases the possibilities for using the urban space. Tallinn is a capital city with one of the cleanest air and lowest noise levels in the world. The Tallinn region is viewed as a whole and the borders between the city and the neighbouring municipalities do not disrupt this comprehensive space.

<ReadMore id="annex-1-1" />

<LightBox previewImageUrl="https://img.youtube.com/vi/Akf4uzf6lg8/hqdefault.jpg">
  <iframe
    width="100%"
    src="https://www.youtube.com/embed/Akf4uzf6lg8?autoplay=1&yt:cc=on&hl=en&cc_lang_pref=en&cc_load_policy=1"
    frameborder="0"
    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
    allowfullscreen
  ></iframe>
</LightBox>

</Hero>

<Content>

## Spatially cohesive urban region

<MapTrigger id="S02-keskused" isDefault={true}>

The Tallinn region forms an integrated urban region, an area for business and everyday activities whose backbone is a network of centres. There are unique and multi-purpose residential areas in and around the centres. Central Tallinn is the most active centre in the region and is also the easiest to access by public transport. The settlement areas surrounding Tallinn have become intertwined with the city and the spatial transitions are smooth and smart. The street network, the cycle lane network and the green areas connect all regions. Denser development areas emerge by regional centres and public transport hubs near Tallinn and the railway transport opportunities are also used in these areas.

<ReadMore id="annex-1-2" />

</MapTrigger>

## Inviting heart of the city

<ImageTrigger id="tanavaskeem.gif" alt="">

The centre of Tallinn is a city centre with an accessible street space, high-level architecture and diverse possibilities. It is an attractive place for leisure, work and living. The street space is designed so that it is good to be outdoors in all kinds of weather: there are trees, shelters, places for sitting or having chats while standing. The well-designed street network and the low speed of vehicles invite people to choose between several routes for their daily journeys and offer the joy of discovery, giving people the chance to walk in parks and visit courtyards, squares and the seaside. The façades of new buildings are varied. Street cafés, small shops and service establishments are located on the ground floors of buildings. Every subdistrict in the city centre is unique. It is easy to reach the centre by public transport, by walking or cycling, which decreases the need for car use in the city centre.

<ReadMore id="annex-1-3" />

</ImageTrigger>

## 15-minute city

<MapTrigger id="S04-tombekeskused">

Tallinn is a city with many centres where everyday activities and services are within a 15-minute walk for most citizens. Tallinn has larger and smaller centres that have become important places for everyday communication – they are characterised by a high-quality public space and a multitude of activity opportunities and people. There are three types of centres: citywide, district and subdistrict. All of these unique centres are hubs of public space, which are special and attractive places. They have a human scale design, are easily accessible by various forms of mobility and function in harmony with the city centre. Places of residence, workplaces and services are concentrated in larger centres. These are also gateways to the fast public transportation network. One centre may represent several types: it may attract people from all over the world, it may be the heart of the city district and it may be a meeting place within the community. More centres emerge as the city develops. When shopping centres are developed, it is kept in mind that they are areas of good public space which connect the paths people use for walking instead of dividing them.

<Meter title="By 2035">

The share of people residing in **the catchment areas** of centres will increase

![Illustration of the centers and their areas of influence](../meedia/moodikud/S03.svg)

<!-- <MeterData>


2020: **75%**

</MeterData> -->

</Meter>

<ReadMore id="annex-1-4" />

</MapTrigger>

## Shared streets

<MapTrigger id="S06-tanavatuubid">

A city street is not only a means of mobility, but also a pleasant environment full of diverse possibilities. A well-designed space reduces speeding and other inconsiderate actions. Tallinn streets are designed by their place value, which takes into consideration the nature of the street space, its importance to pedestrians, cyclists and public transport and the safe organisation of road traffic. It is possible to find pleasant places for stopping, spending time and even working in the public urban space. There are many roads and streets in the city that can be used for several forms of mobility, which offer a change in everyday routes and allow people to discover new places and meet people in the city. Convenient, fast and frequent public transport has been organised on the most important arterial roads. The more local a street is, the calmer its design. Different requirements apply to pedestrian and bicycle traffic, public transport capacity and road maintenance on different types of streets. The pavements important to pedestrians and the core network of cycle lanes are maintained as a priority.

<Meter>

![Place, street and road](../meedia/moodikud/S051_eng.svg)

</Meter>

<Meter>

![Place](../meedia/moodikud/S052_eng.svg)

</Meter>

<Meter>

![Street](../meedia/moodikud/S053_eng.svg)

</Meter>

<Meter>

![Road](../meedia/moodikud/S054_eng.svg)

</Meter>

<ReadMore id="annex-1-5" />

</MapTrigger>

## Green urban space everywhere

<MapTrigger id="S09-rohealad">

Greenery can be seen everywhere in the urban space – Tallinn is known for its beautiful avenues, green corridors, parks and urban forests. The people of Tallinn live, study and work within a 4-5-minute walk from the nearest streamlined green and landscaped area. There is greenery in the streets, yards and squares and on buildings: taller and shorter vegetation, container gardens and green walls are used. Greenery is also used in car parks, and pavements with good water permeability are preferred. Industrial wastelands and wild shrublands are in intermediate use, which creates an exciting contrast in the streamlined urban space. People experiment with various ideas, such as combining biology, space and digital solutions. There are multifunctional landscapes in the city, where food production, biodiversity and other natural benefits are combined.

<Meter title="By 2035">

The majority of journeys made by **70%** of residents will be green

![Illustration](../meedia/moodikud/S08.svg)

<MeterData>

2020: **62%**

</MeterData>

</Meter>

<ReadMore id="annex-1-6" />

</MapTrigger>

## A city open to the sea

<ImageTrigger id="1_merele.jpg" alt="">

The sea is well perceivable in the city and the diverse coastal areas and small harbours are popular places for recreational activities among the citizens and the city's guests alike. The seaside is open and unobstructed and can be accessed via many streets and paths. Tallinn is a well-known port city with a network of harbours with different functions. The coast of Tallinn Bay is an exciting experience consisting of interesting urban spaces and eye-catching architecture, the harbours, natural beaches and parks and natural areas of minimal human interference. The promenade connecting the beaches and subdistricts is accessible and uninterrupted; attracting attention with its varied design, it also offers citizens many activities. All of this can be seen by going on a sailing trip on Tallinn Bay. Maritime culture, including the culture of seafaring, is part of Tallinn.

<ReadMore id="annex-1-7" />

</ImageTrigger>

</Content>

<DomainsGrid>

## Key fields

<Domain id="urban-planning">
  A strong urban planning vision will create a pleasant urban space that takes
  into consideration the interests of all parties and makes people’s lives
  easier. A high-quality public space and architecture, various architectural
  forms and spectacular greenery make Tallinn a desirable place to visit and
  live in. During the planning process, directions will be given to all parties
  shaping our urban space on how to create a friendly and homey urban space.
</Domain>

<Domain id="mobility">
  The increased share of sustainable mobility and safe traffic, decreased
  traffic management and implementing road surfaces made from various materials
  will make the urban space become human-scale. Sustainability is essential for
  use of urban space — the current car parks and intersections will be turned
  into pleasant public spaces. The balance of various mobility options enables
  the space to become more interesting and safe.
</Domain>

<Domain id="urban-landscape">
  Tallinn’s green network keeps Tallinn’s urban nature diverse and creates spots
  where people can practice healthy habits and move around. The cohesion of the
  green spaces will allow people to travel almost from one end of the city to
  the other by bike or on foot so that their contact with car traffic will be
  minimal. A well-maintained and diverse landscape means green spaces are more
  accessible and attractive to people – the urban space will become a more
  pleasant place.
</Domain>

<Domain id="city-property">
  Owning the rights or land needed for the development of the urban space, the
  smart planning of the city’s real estate environment as well as cooperation
  help to shape diverse and attractive hubs. This way, the urban property will
  be maintained and developed responsibly and thought out strategically so that
  the creation of a friendly urban space would not get stuck due to absence of
  real estate.
</Domain>

## Contributing fields

<Domain id="business-environment">
  This urban environment fosters the emergence of unique commerce and service
  companies, as well as cultural events and entertainment options that both city
  residents and visitors can participate in. Tourists are invaluable clients for
  companies operating in the service and culture fields and our goal is to
  attract them to visit all the districts, not only the Tallinn Old Town, in the
  name of sustainability. All this enriches and livens the urban space.
</Domain>

<Domain id="culture">
  Cultural establishments and events will make the urban space more interesting.
  They will encourage people to spend more time in the city and, thus, there
  will be more people in the urban space which will make the city more
  attractive.
</Domain>

<Domain id="social-welfare">
  By supporting independent coping and offering social welfare aids, we help to
  ensure that people are both mentally and physically healthy and live longer.
  An accessibility policy ensures that the urban space will also be accessible
  to those who need assistance aids to move around.
</Domain>

<Domain id="utility-networks">
  A compact utility network will help with shaping landscaping and other
  aboveground infrastructure and developing an attractive urban space.
  Well-thought out street lighting helps to create a safe and pleasant urban
  space.
</Domain>

</DomainsGrid>
