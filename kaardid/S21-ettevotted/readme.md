Märkused:
Arvestatud on vaid hooned, mis on katergooriseeritud kas 
2 Ühiskondlik hoone - Hoone, mis on avalikus kasutuses.
või
4 Tootmishoone - Elamiseks mittekasutatav hoone, mis ei ole avalikus kasutuses.
Suletud Bruto pind on pindala korrutatud korrustega.
Iga hoone kohta on loodud üks punkt. 
Kui punkt asub ruudus siis antakse kogu väärtus ruudule.

Parameetrid

	SuletudBruto_Sum : Kumulatiivne suletud bruto pind ruutmeetrites ruudu kohta. 
	
Parameetrid, mis ei ole visualiiritavad:

	GRD_INSPIR : Ruudu ID